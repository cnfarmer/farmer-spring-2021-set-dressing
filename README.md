## Set Dressing Project
February 8, 2021
Description: Basic 3D set dressing for exploratory game. 

## Implementation (Techniques used for player navigation)
Breadcrumbing
Site lines
Lighting Contrasts
Elevation Changes

## References
Videos from class as well as in class explanations

## Future Development
None currently

## Created by
Cierra Farmer